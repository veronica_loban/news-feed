import "./style.css";
let renderArray = [];
let messageId = 0;

class Post {
    constructor(messageId, author, date, text) {
        this.id = messageId;
        this.author = author;
        this.date = date;
        this.text = text;
        this.likes = 0;

        renderArray.push(this);
        localStorage.setItem('post', JSON.stringify(renderArray));

    }

    render() {
        let message_list = document.getElementById('message_list');
        this.newLi = document.createElement('li');
        this.newLi.innerHTML = `
                    <div class="post__author">
                        <span class="span_author"><b>${this.author}</b></span>
                        </div>
                    <div class="post_date">
                        <span><b>${this.date}</b></span>
                    </div>
                    <div class="post__text">
                         <span class="span_text">${this.text}</span>
                    </div>
                        <div class="post_likes">
                    <button class="button">Likes ${this.likes}</button> 
                        </div>
                    `;

        message_list.appendChild(this.newLi);

        let buttonHTMLCollection = this.newLi.getElementsByClassName('button');
        let button = Array.from(buttonHTMLCollection);

        button.forEach((item) => {
            item.addEventListener('click', () => {
                this.likes += 1;
                item.innerHTML = `Likes ${this.likes}`
            });
        })
    }
}

const messageForm = document.getElementById('message');
let authors = document.getElementById('author');
let textArea = messageForm.elements.message;
let date = new Date();
let i = 0;

const RenderFeed  = ( arr )  => {
    arr.map ( item => {
        item.render()
    })
};

RenderFeed( renderArray );

let resetForm = () => {
    messageForm.reset();
};

messageForm.addEventListener('submit', (e) => {

    e.preventDefault();

    let author = authors.value;
    let text = textArea.value;
    let post_Date = () => {return `${date.getDate()}/${1 + date.getMonth()}/${date.getFullYear()}`};

    let jsonArray = JSON.parse(localStorage.getItem('renderArray')  );
    if( localStorage.getItem('renderArray') !== null ){
        let renderArray = jsonArray.map( item => new Post(item) );
    } else {
        let renderArray = [];
    }


    let post = new Post (messageId++, author, post_Date(), text);

    post.render();
    resetForm();
});
