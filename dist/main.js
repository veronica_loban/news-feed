/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _style_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.css */ \"./src/style.css\");\n/* harmony import */ var _style_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\nlet renderArray = [];\r\nlet messageId = 0;\r\n\r\nclass Post {\r\n    constructor(messageId, author, date, text) {\r\n        this.id = messageId;\r\n        this.author = author;\r\n        this.date = date;\r\n        this.text = text;\r\n        this.likes = 0;\r\n\r\n        renderArray.push(this);\r\n        localStorage.setItem('post', JSON.stringify(renderArray));\r\n\r\n    }\r\n\r\n    render() {\r\n        let message_list = document.getElementById('message_list');\r\n        this.newLi = document.createElement('li');\r\n        this.newLi.innerHTML = `\r\n                    <div class=\"post__author\">\r\n                        <span class=\"span_author\"><b>${this.author}</b></span>\r\n                        </div>\r\n                    <div class=\"post_date\">\r\n                        <span><b>${this.date}</b></span>\r\n                    </div>\r\n                    <div class=\"post__text\">\r\n                         <span class=\"span_text\">${this.text}</span>\r\n                    </div>\r\n                        <div class=\"post_likes\">\r\n                    <button class=\"button\">Likes ${this.likes}</button> \r\n                        </div>\r\n                    `;\r\n\r\n        message_list.appendChild(this.newLi);\r\n\r\n        let buttonHTMLCollection = this.newLi.getElementsByClassName('button');\r\n        let button = Array.from(buttonHTMLCollection);\r\n\r\n        button.forEach((item) => {\r\n            item.addEventListener('click', () => {\r\n                this.likes += 1;\r\n                item.innerHTML = `Likes ${this.likes}`\r\n            });\r\n        })\r\n    }\r\n}\r\n\r\nconst messageForm = document.getElementById('message');\r\nlet authors = document.getElementById('author');\r\nlet textArea = messageForm.elements.message;\r\nlet date = new Date();\r\nlet i = 0;\r\n\r\nconst RenderFeed  = ( arr )  => {\r\n    arr.map ( item => {\r\n        item.render()\r\n    })\r\n};\r\n\r\nRenderFeed( renderArray );\r\n\r\nlet resetForm = () => {\r\n    messageForm.reset();\r\n};\r\n\r\nmessageForm.addEventListener('submit', (e) => {\r\n\r\n    e.preventDefault();\r\n\r\n    let author = authors.value;\r\n    let text = textArea.value;\r\n    let post_Date = () => {return `${date.getDate()}/${1 + date.getMonth()}/${date.getFullYear()}`};\r\n\r\n    let jsonArray = JSON.parse(localStorage.getItem('renderArray')  );\r\n    if( localStorage.getItem('renderArray') !== null ){\r\n        let renderArray = jsonArray.map( item => new Post(item) );\r\n    } else {\r\n        let renderArray = [];\r\n    }\r\n\r\n\r\n    let post = new Post (messageId++, author, post_Date(), text);\r\n\r\n    post.render();\r\n    resetForm();\r\n});\r\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/style.css":
/*!***********************!*\
  !*** ./src/style.css ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// removed by extract-text-webpack-plugin\n\n//# sourceURL=webpack:///./src/style.css?");

/***/ })

/******/ });